#pragma once

#include "Keyboard.h"


class Input
{
public:
	Input()	:
		sdlEvent(),
		keyboard(&sdlEvent)
	{
		clearSinglePressData = true;
	}

	bool PollEvent();

	SDL_Event sdlEvent;
	Keyboard  keyboard;

private:
	bool clearSinglePressData;
};