#include "Renderer.h"
#include "Renderer.h"
#include <stdio.h>

#include "SDL_image.h"
#include "Texture.h"


Renderer::Renderer(Window& window):
	window(window),
	sdlRenderer(NULL)
{
	sdlRenderer = SDL_CreateRenderer(window.GetWindow(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if (sdlRenderer == NULL)
	{
		printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
	}
	else
	{
		//Initialize renderer color
		SDL_SetRenderDrawColor(sdlRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

		if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) == 0)
		{
			printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
		}
	}
}

Renderer::~Renderer()
{

}

SDL_Renderer* Renderer::GetRenderer() const
{
	return sdlRenderer;
}

void Renderer::Clear(byte r, byte g, byte b)
{
	SDL_SetRenderDrawColor(sdlRenderer, r, g, b, 255);
	SDL_RenderClear(sdlRenderer);
}

void Renderer::Present()
{
	SDL_RenderPresent(sdlRenderer);
}

void Renderer::Draw(Texture* texture, const Vector2f& position) const
{
	SDL_Rect renderQuad = { static_cast<int>(position.x), static_cast<int>(position.y), texture->GetSize().x, texture->GetSize().y };

	SDL_RenderCopy(sdlRenderer, texture->GetTexture(), NULL, &renderQuad);
}

Window& Renderer::GetWindow() const
{
	return window;
}
