/*
    VMMLib - Vector & Matrix Math Lib

    @author Jonas Boesch
    @author Stefan Eilemann
    @author Renato Pajarola
    @author David H. Eberly ( Wild Magic )
    @author Andrew Willmott ( VL )
    @author Vladimir Bondarev ( Matrix2x3 )

    @license revised BSD license, check LICENSE

    parts of the source code of VMMLib were inspired by David Eberly's
    Wild Magic and Andrew Willmott's VL.

*/

#ifndef __VMML__MATRIX2x3__H__
#define __VMML__MATRIX2x3__H__

/*
    2x3 Matrix Class
*/

#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <cassert>
#include <string>
#include <vector>

// * * * * * * * * * *
//
// - declaration -
//
// * * * * * * * * * *

namespace vmml
{
	template< typename T > class Vector2;

	template< typename T >
	class Matrix2x3
	{
	public:
		union
		{
			struct
			{
				/**
				    The matrix element in 'math' notation, e.g. similar to the way
				    it is done in mathematical texts.

				    The first index denotes the row, the second the column. This
				    means that m21 is the element at row #2, col #1.
				*/
				T   m00, m10,
				m01, m11,
				m02, m12;
			};
			struct
			{
				T   rot00, rot10,
				rot01, rot11,
				x,     y;
			};

			/**
			    The linear / 1d-array representation of the matrix. Useful for
			    usage with the OpenGL API. E.g.: glLoadMatrix( mymat.ml );
			*/
			T ml[6];
			T array[6];

			/**
			    The following representation is for internal purposes.
			    The first array index specifies the __column__ number, the second
			    the row: m[col][row].

			    Example: m[2][1] -> row #1, col #2
			*/
			T m[2][3];
		};

		Matrix2x3();
		Matrix2x3(T v00, T v01,
				  T v10, T v11,
				  T v20, T v21);

		Matrix2x3(const Matrix2x3& other);
		//type conversion ctor
		template< typename U >
		Matrix2x3(const Matrix2x3< U >& other);

		inline const Matrix2x3& operator= (const Matrix2x3& other);
		template< typename U >
		inline const Matrix2x3& operator= (const Matrix2x3< U >& other);

		// vector = matrix * vector
		Vector2< T > operator* (const Vector2< T >& other) const;
		Matrix2x3 operator* (const Matrix2x3& other) const;

		/** create rotation matrix from parameters.
		    @param angle - angle in radians
		    @param rotation axis - must be normalized!
		*/
		void rotateZ(const T angle);
		void scale(const T x, const T y);
		void scale(const Vector2< T >& scale_);

		void scaleTranslation(const T x, const T y);
		void scaleTranslation(const Vector2< T >& scale_);

		void setTranslation(const T x, const T y);
		void setTranslation(const Vector2< T >& trans);
		Vector2< T > getTranslation() const;

		static const Matrix2x3 IDENTITY;
		static const Matrix2x3 ZERO;

		void PrintMatrix()
		{
			printf("Matrix: [%2.2f, %2.2f],\n[%2.2f, %2.2f],\n[%2.2f, %2.2f]\n",
				   m00, m10,
				   m01, m11,
				   m02, m12);
		}
	}; // class matrix2x3

	typedef Matrix2x3< float >  Matrix2x3f;
	typedef Matrix2x3< double > Matrix2x3d;
} // namespace vmml

// * * * * * * * * * *
//
// - implementation -
//
// * * * * * * * * * *
#include "VMMLib/vector2.h"

namespace vmml
{
	template< typename T >
	const Matrix2x3< T > Matrix2x3< T >::IDENTITY(1, 0,
			0, 1,
			0, 0);

	template< typename T >
	const Matrix2x3< T > Matrix2x3< T >::ZERO(0, 0,
			0, 0,
			0, 0);

	template< typename T >
	Matrix2x3< T >::Matrix2x3()
	{}

	template< typename T >
	Matrix2x3< T >::Matrix2x3(T v00, T v10,
							  T v01, T v11,
							  T v02, T v12)
		: m00(v00)
		, m10(v10)
		, m01(v01)
		, m11(v11)
		, m02(v02)
		, m12(v12)
	{}

	template< typename T >
	Matrix2x3< T >::Matrix2x3(const Matrix2x3& other)
	{
		memcpy(ml, other.ml, 6 * sizeof(T));
	}

	template< typename T >
	template< typename U >
	Matrix2x3< T >::Matrix2x3(const Matrix2x3< U >& other)
	{
		for (size_t i = 0; i < 6; ++i)
		{
			ml[i] = static_cast< T >(other.ml[i]);
		}
	}

	template< typename T >
	const Matrix2x3< T >& Matrix2x3< T >::operator= (const Matrix2x3< T >& other)
	{
		memcpy(ml, other.ml, 6 * sizeof(T));
		return *this;
	}

	template< typename T >
	template< typename U >
	const Matrix2x3< T >& Matrix2x3< T >::operator= (const Matrix2x3< U >& other)
	{
		ml[  0 ] = static_cast< T >(other.ml[  0 ]);
		ml[  1 ] = static_cast< T >(other.ml[  1 ]);
		ml[  2 ] = static_cast< T >(other.ml[  2 ]);
		ml[  3 ] = static_cast< T >(other.ml[  3 ]);
		ml[  4 ] = static_cast< T >(other.ml[  4 ]);
		ml[  5 ] = static_cast< T >(other.ml[  5 ]);
		return *this;
	}

	template< typename T >
	Vector2< T > Matrix2x3< T >::operator* (const Vector2< T >& other) const
	{
		return Vector2< T >(other[0] * rot00 +
							other[1] * rot01 + x,

							other[0] * rot10 +
							other[1] * rot11 + y);
	}

	template< typename T >
	void Matrix2x3<T>::rotateZ(const T angle)
	{
		//matrix multiplication: ml = ml * rotation z axis
		const T sinus = sin(angle);
		const T cosin = cos(angle);

		T tmp	=  m00 * cosin + m01 * sinus;
		m01		= -m00 * sinus + m01 * cosin;
		m00		= tmp;

		tmp		=  m10 * cosin + m11 * sinus;
		m11		= -m10 * sinus + m11 * cosin;
		m10		=  tmp;
	}

	template<>
	inline void Matrix2x3<float>::rotateZ(const float angle)
	{
		//matrix multiplication: ml = ml * rotation z axis
		const float sinus = sinf(angle);
		const float cosin = cosf(angle);

		float tmp	=  m00 * cosin + m01 * sinus;
		m01			= -m00 * sinus + m01 * cosin;
		m00			= tmp;

		tmp			=  m10 * cosin + m11 * sinus;
		m11			= -m10 * sinus + m11 * cosin;
		m10			=  tmp;
	}

	template< typename T >
	void Matrix2x3<T>::scale(const T xScale, const T yScale)
	{
		ml[0]  *= xScale;
		ml[1]  *= xScale;

		ml[2]  *= yScale;
		ml[3]  *= yScale;
	}

	template< typename T >
	void Matrix2x3<T>::scale(const Vector2< T >& scale_)
	{
		ml[0]  *= scale_[0];
		ml[1]  *= scale_[0];

		ml[2]  *= scale_[1];
		ml[3]  *= scale_[1];
	}

	template< typename T >
	void Matrix2x3<T>::scaleTranslation(const Vector2< T >& scale_)
	{
		ml[4] *= scale_[0];
		ml[5] *= scale_[1];
	}

	template< typename T >
	void Matrix2x3<T>::setTranslation(const T xTrans, const T yTrans)
	{
		ml[4] = xTrans;
		ml[5] = yTrans;
	}

	template< typename T >
	void Matrix2x3<T>::setTranslation(const Vector2<T>& trans)
	{
		ml[4] = trans.x;
		ml[5] = trans.y;
	}

	template< typename T >
	Vector2< T >
	Matrix2x3< T >::getTranslation() const
	{
		return Vector2< T > (ml[4], ml[5]);
	}
} // namespace vmml

#endif
