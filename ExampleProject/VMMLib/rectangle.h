/*
    VMMLib - Vector & Matrix Math Lib

    @author Maurice Sibrandi

    @license revised BSD license, check LICENSE
*/

#ifndef __VMML__RECTANGLE__H__
#define __VMML__RECTANGLE__H__

// - declaration -

namespace vmml
{
	template<typename T>
	class Rectangle
	{
	public:
		Vector2<T> position;
		Vector2<T> size;

		// constructors
		Rectangle();
		Rectangle(const T a);
		Rectangle(const T x, const T y, const T width, const T height);
		Rectangle(const Vector2<T>& position, const Vector2<T>& size);
		Rectangle(const Rectangle<T>& r);
		~Rectangle();

		template< typename U >
		Rectangle(const Rectangle< U >& a);

		Rectangle<T>& operator=(const Rectangle<T>& r);

		bool operator==(const Rectangle<T>& r) const;
		bool operator!=(const Rectangle<T>& r) const;

		Rectangle<T> operator&(const Rectangle<T>& other);

		void set(const T x, const T y, const T width, const T height);
		void set(const Vector2<T>& position, const Vector2<T>& size);
		void Set(const T x, const T y, const T width, const T height);
		void Set(const Vector2<T>& position, const Vector2<T>& size);

		void setPosition(const T x, const T y);
		void setPosition(const Vector2<T>& position);
		void SetPosition(const T x, const T y);
		void SetPosition(const Vector2<T>& position);

		void setSize(const T width, const T height);
		void setSize(const Vector2<T>& size);
		void SetSize(const T width, const T height);
		void SetSize(const Vector2<T>& size);

		Vector2<T> Center() const;
		T CenterX() const;
		T CenterY() const;
		T left() const;
		T top() const;
		T right() const;
		T bottom() const;
		T Left() const;
		T Top() const;
		T Right() const;
		T Bottom() const;

		Vector2f Centerf() const;
		Vector2f Sizef() const;

		bool Contains(const T x, const T y) const;
		bool Contains(const Vector2<T>& point) const;
		bool contains(const T x, const T y) const;
		bool contains(const Vector2<T>& point) const;

		void Inflate(const T a);
		void Inflate(const T x, const T y);
		void Inflate(const Vector2<T> size);
		void inflate(const T a);
		void inflate(const T x, const T y);
		void inflate(const Vector2<T> size);

		bool Intersects(Rectangle<T> other) const;
		bool intersects(Rectangle<T> other) const;

		Rectangle<T> Intersect(const Rectangle<T> other) const;

		static const Rectangle<T> EMPTY;
		static const Rectangle<T> ZERO;
	};

	template<typename T>
	Rectangle<T> vmml::Rectangle<T>::Intersect(const Rectangle<T> other) const
	{
		EE_ASSERT(Intersects(other));
		Rectangle<T> out;
		out.position.x = max(position.x, other.position.x);
		out.position.y = max(position.y, other.position.y);
		T b = min(Bottom(), other.Bottom());
		T r = min(Right(), other.Right());
		out.size.x = r - out.position.x;
		out.size.y = b - out.position.y;
		return out;
	}

	typedef Rectangle<unsigned char>    Rectangleub;
	typedef Rectangle<char>				Rectangleb;
	typedef Rectangle<int>              Rectanglei;
	typedef Rectangle<float>            Rectanglef;
	typedef Rectangle<double>           Rectangled;
}

// - implementation - //

namespace vmml
{
	template<typename T>
	const Rectangle<T> Rectangle<T>::EMPTY = Rectangle<T>(0, 0, -1, -1);

	template<typename T>
	const Rectangle<T> Rectangle<T>::ZERO(0, 0, 0, 0);

	template<typename T>
	Rectangle<T>::Rectangle()
	{
	}

	template<typename T>
	Rectangle<T>::Rectangle(const T a)
		: position(a)
		, size(a)
	{
	}

	template<typename T>
	Rectangle<T>::Rectangle(const T x, const T y, const T width, const T height)
		: position(x, y)
		, size(width, height)
	{
	}

	template<typename T>
	Rectangle<T>::Rectangle(const Vector2<T>& _position, const Vector2<T>& _size)
		: position(_position)
		, size(_size)
	{
	}

	template<typename T>
	Rectangle<T>::Rectangle(const Rectangle<T>& r)
		: position(r.position)
		, size(r.size)
	{
	}

	template< typename T >
	template< typename U >
	vmml::Rectangle<T>::Rectangle(const Rectangle< U >& other)
		: position(other.position)
		, size(other.size)
	{
	}

	template<typename T>
	Rectangle<T>::~Rectangle()
	{
	}

	template<typename T>
	Rectangle<T>& Rectangle<T>::operator=(const Rectangle<T>& r)
	{
		position = r.position;
		size = r.size;
		return *this;
	}

	template<typename T>
	bool Rectangle<T>::operator==(const Rectangle<T>& r) const
	{
		if (position.x != r.position.x)
		{
			return false;
		}
		if (position.y != r.position.y)
		{
			return false;
		}

		if (size.x != r.size.x)
		{
			return false;
		}
		if (size.y != r.size.y)
		{
			return false;
		}

		return true;
	}

	template<typename T>
	bool Rectangle<T>::operator!=(const Rectangle<T>& r) const
	{
		if (position.x != r.position.x)
		{
			return true;
		}
		if (position.y != r.position.y)
		{
			return true;
		}

		if (size.x != r.size.x)
		{
			return true;
		}
		if (size.y != r.size.y)
		{
			return true;
		}

		return false;
	}

	template<typename T>
	void Rectangle<T>::set(const T x, const T y, const T width, const T height)
	{
		position.x = x;
		position.y = y;
		size.x = width;
		size.y = height;
	}

	template<typename T>
	void Rectangle<T>::set(const Vector2<T>& position, const Vector2<T>& size)
	{
		this->position = position;
		this->size = size;
	}

	template<typename T>
	void Rectangle<T>::setPosition(const T x, const T y)
	{
		position.x = x;
		position.y = y;
	}

	template<typename T>
	void Rectangle<T>::setPosition(const Vector2<T>& position)
	{
		this->position = position;
	}

	template<typename T>
	void Rectangle<T>::setSize(const T width, const T height)
	{
		size.x = width;
		size.y = height;
	}

	template<typename T>
	void Rectangle<T>::setSize(const Vector2<T>& size)
	{
		this->size = size;
	}


	template<typename T>
	void Rectangle<T>::Set(const T x, const T y, const T width, const T height)
	{
		position.x = x;
		position.y = y;
		size.x = width;
		size.y = height;
	}

	template<typename T>
	void Rectangle<T>::Set(const Vector2<T>& position, const Vector2<T>& size)
	{
		this->position = position;
		this->size = size;
	}

	template<typename T>
	void Rectangle<T>::SetPosition(const T x, const T y)
	{
		position.x = x;
		position.y = y;
	}

	template<typename T>
	void Rectangle<T>::SetPosition(const Vector2<T>& position)
	{
		this->position = position;
	}

	template<typename T>
	void Rectangle<T>::SetSize(const T width, const T height)
	{
		size.x = width;
		size.y = height;
	}

	template<typename T>
	void Rectangle<T>::SetSize(const Vector2<T>& size)
	{
		this->size = size;
	}
	template<typename T>
	bool Rectangle<T>::contains(const T x, const T y) const
	{
		return x >= left() && x <= right() && y >= top() && y <= bottom();
	}

	template<typename T>
	bool Rectangle<T>::contains(const Vector2<T>& point) const
	{
		T x = point.x;
		T y = point.y;
		return x >= left() && x <= right() && y >= top() && y <= bottom();
	}

	template<typename T>
	bool Rectangle<T>::Contains(const T x, const T y) const
	{
		return x >= left() && x <= right() && y >= top() && y <= bottom();
	}

	template<typename T>
	bool Rectangle<T>::Contains(const Vector2<T>& point) const
	{
		T x = point.x;
		T y = point.y;
		return x >= left() && x <= right() && y >= top() && y <= bottom();
	}

	template<typename T>
	void Rectangle<T>::inflate(const T a)
	{
		position -= a;
		size += 2 * a;
	}

	template<typename T>
	void Rectangle<T>::inflate(const T x, const T y)
	{
		Vector2<T> tmp(x, y);
		position -= tmp;
		size += 2 * tmp;
	}

	template<typename T>
	void Rectangle<T>::inflate(const Vector2<T> size)
	{
		position -= size;
		this->size.x += 2.0f * size.x;
		this->size.y += 2.0f * size.y;
	}


	template<typename T>
	void Rectangle<T>::Inflate(const T a)
	{
		position -= a;
		size += 2 * a;
	}

	template<typename T>
	void Rectangle<T>::Inflate(const T x, const T y)
	{
		Vector2<T> tmp(x, y);
		position -= tmp;
		size += tmp * 2;
	}

	template<typename T>
	void Rectangle<T>::Inflate(const Vector2<T> size)
	{
		position -= size;
		this->size.x += 2.0f * size.x;
		this->size.y += 2.0f * size.y;
	}

	template<typename T>
	bool Rectangle<T>::intersects(Rectangle<T> other) const
	{
		return !(left() > other.right() || right() < other.left() ||
				 top() > other.bottom() || bottom() < other.top());
	}

	template<typename T>
	bool Rectangle<T>::Intersects(Rectangle<T> other) const
	{
		return !(left() > other.right() || right() < other.left() ||
				 top() > other.bottom() || bottom() < other.top());
	}

	template<typename T>
	Rectangle<T> Rectangle<T>::operator&(const Rectangle<T>& other)
	{
		assert(true && "This function hasn't been tested yet!");

		if (intersects(other))
		{
			Vector2<T> pos = Vector2<T>(max(position.x, other.position.x), 	max(position.y, other.position.y));
			Vector2<T> size;
			size.x = min(right(), other.right()) - pos.x;
			size.y = min(bottom(), other.bottom()) - pos.y;

			return Rectangle<T>(pos, size);
		}

		return ZERO;
	}

	template<typename T>
	Vector2<T> Rectangle<T>::Center() const
	{
		return position + size / 2;
	}

	template<typename T>
	T Rectangle<T>::CenterX() const
	{
		return position.x + size.x / 2;
	}

	template<typename T>
	T Rectangle<T>::CenterY() const
	{
		return position.y + size.y / 2;
	}

	template<typename T>
	T Rectangle<T>::left() const
	{
		return position.x;
	};

	template<typename T>
	T Rectangle<T>::top() const
	{
		return position.y;
	};

	template<typename T>
	T Rectangle<T>::right() const
	{
		return position.x + size.x;
	};

	template<typename T>
	T Rectangle<T>::bottom() const
	{
		return position.y + size.y;
	};

	template<typename T>
	T Rectangle<T>::Left() const
	{
		return position.x;
	};

	template<typename T>
	T Rectangle<T>::Top() const
	{
		return position.y;
	};

	template<typename T>
	T Rectangle<T>::Right() const
	{
		return position.x + size.x;
	};

	template<typename T>
	T Rectangle<T>::Bottom() const
	{
		return position.y + size.y;
	};

	template<typename T>
	Vector2f Rectangle<T>::Centerf() const
	{
		return Vector2f((float)position.x + (float)size.x / 2.0f, (float)position.y + (float)size.y / 2.0f);
	};


	template<typename T>
	Vector2f vmml::Rectangle<T>::Sizef() const
	{
		return (Vector2f)size;
	}

}

#endif
