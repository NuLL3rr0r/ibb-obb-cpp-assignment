/**
 * @file
 *
 * @section DESCRIPTION
 *
 * Low-level keyboard input handler
 */

#pragma once

#include <SDL.h>

#include <unordered_map>

class Keyboard
{
public:
	Keyboard(SDL_Event* event);

	bool IsKeyPressed(SDL_Keycode key);

	bool IsKeyReleased(SDL_Keycode key);

	bool OnKeyPress(SDL_Keycode key);

	bool OnKeyRelease(SDL_Keycode key);

	void Clear();

	void Update();
private:
	SDL_Event* event;
	std::unordered_map<int, bool> keys;
	std::unordered_map<int, bool> keysPressed;
	std::unordered_map<int, bool> keysReleased;
};