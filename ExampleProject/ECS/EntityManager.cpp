/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The entitny manager
 */

#include "EntityManager.h"
#include "ECS.h"
#include "Entity.h"
#include "Renderer.h"

struct EntityManager::Impl
{
    ECS::EntityVector Entities;
};

EntityManager::EntityManager() :
    pimpl(std::make_unique<EntityManager::Impl>())
{

}

EntityManager::~EntityManager() = default;

ECS::EntityVector& EntityManager::GetEntities() const
{
    return pimpl->Entities;
}

void EntityManager::Init()
{
    for (auto& e : pimpl->Entities)
    {
        e->Init();
    }
}

void EntityManager::Update(float delta)
{
    for (auto& e : pimpl->Entities)
    {
        e->Update(delta);
    }
}

void EntityManager::Draw(const Renderer& renderer)
{
    for (auto& e : pimpl->Entities)
    {
        e->Draw(renderer);
    }
}

void EntityManager::Refresh()
{
    pimpl->Entities.erase(std::remove_if(std::begin(pimpl->Entities), std::end(pimpl->Entities),
        [](const std::unique_ptr<Entity>& entity)
        {
            return !entity->IsActive();
        }),
        std::end(pimpl->Entities));
}