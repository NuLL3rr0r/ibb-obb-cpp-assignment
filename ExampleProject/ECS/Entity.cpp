/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Abstract base class for all entities
 */

#include "Entity.h"
#include "Component.h"
#include "Game.h"
#include "Renderer.h"

struct Entity::Impl
{
    Game* Game;

    bool bActive;

    ECS::ComponentVector Components;
    ECS::ComponentArray ComponentArray;
    ECS::ComponentBitSet ComponentBitSet;
};

Entity::Entity(Game* game) :
    pimpl(std::make_unique<Entity::Impl>())
{
    pimpl->Game = game;
    pimpl->bActive = false;
}

Entity::~Entity() = default;

bool Entity::IsActive() const
{
    return pimpl->bActive;
}

ECS::ComponentVector& Entity::GetComponents() const
{
    return pimpl->Components;
}

ECS::ComponentArray& Entity::GetComponentArray() const
{
    return pimpl->ComponentArray;
}

ECS::ComponentBitSet& Entity::GetComponentBitSet() const
{
    return pimpl->ComponentBitSet;
}

void Entity::Init()
{
    for (auto& c : pimpl->Components)
    {
        c->Init();
    }
}

Game* Entity::GetGameObject() const
{
    return pimpl->Game;
}

void Entity::Update(float delta)
{
    for (auto& c : pimpl->Components)
    {
        c->Update(delta);
    }
}

void Entity::Draw(const Renderer& renderer)
{
    for (auto& c : pimpl->Components)
    {
        c->Draw(renderer);
    }
}

void Entity::Destroy()
{
    pimpl->bActive = false;
}