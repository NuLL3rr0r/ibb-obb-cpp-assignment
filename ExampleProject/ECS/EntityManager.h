/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The entitny manager
 */

#pragma once

#include <memory>
#include "ECS/ECS.h"

class Renderer;

class EntityManager
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	EntityManager();
	virtual ~EntityManager();

public:
	ECS::EntityVector& GetEntities() const;

	template<typename T, typename... TArgs>
	T* AddEntity(TArgs&&... args)
	{
		std::unique_ptr<T> uptr{ std::make_unique<T>(std::forward<TArgs>(args)...) };
		T* e = uptr.get();
		GetEntities().emplace_back(std::move(uptr));
		return e;
	}

public:
	void Init();
	void Update(float delta);
	void Draw(const Renderer& renderer);
	void Refresh();
};

