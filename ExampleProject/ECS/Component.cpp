/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Abstract base class for all components
 */

#include "Component.h"
#include "Entity.h"

struct Component::Impl
{
    Entity* Entity;
};

Component::Component() :
    pimpl(std::make_unique<Component::Impl>())
{
    pimpl->Entity = nullptr;
}

Component::~Component() = default;

Entity* Component::GetEntity() const
{
    return pimpl->Entity;
}

void Component::SetEntity(Entity* entity)
{
    pimpl->Entity = entity;
}

void Component::Init()
{

}

void Component::Update(float delta)
{

}

void Component::Draw(const Renderer& renderer)
{

}