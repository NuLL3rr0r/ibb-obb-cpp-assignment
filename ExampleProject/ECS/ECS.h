/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Generic ECS functions and definitions
 */

#pragma once

#include <array>
#include <bitset>
#include <memory>
#include <vector>
#include "Helpers/Platform.h"

class Component;
class Entity;

namespace ECS
{
	// Maximum number of components
	constexpr std::size_t MAX_COMPONENTS = 32;

	using ComponentID = std::size_t;
	using ComponentUPtr = std::unique_ptr<Component>;
	using ComponentArray = std::array<Component*, MAX_COMPONENTS>;
	using ComponentBitSet = std::bitset<MAX_COMPONENTS>;
	using ComponentVector = std::vector<ComponentUPtr>;

	using EntityUPtr = std::unique_ptr<Entity>;
	using EntityVector = std::vector<EntityUPtr>;

	/// <summary>
	/// You don't have to call this; It meant to be called by the templated
	/// version; Use the templated version instead
	/// </summary>
	/// <returns>The component id</returns>
	FORCEINLINE ComponentID GetComponentTypeID()
	{
		static ComponentID lastID = 0;
		return ++lastID;
	}

	/// <summary>
	/// We use this templated function in order to apply a unique id to each
	/// component type
	/// </summary>
	/// <returns>The component id</returns>
	template<typename T> FORCEINLINE ComponentID GetComponentTypeID() noexcept
	{
		static ComponentID typeID = GetComponentTypeID();
		return typeID;
	}
};