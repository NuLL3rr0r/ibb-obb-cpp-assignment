/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Abstract base class for all components
 */

#pragma once

#include <memory>
#include "ECS.h"

class Renderer;

class Component
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	Component();
	virtual ~Component() = 0;

public:
	Entity* GetEntity() const;
	void SetEntity(Entity* entity);

public:
	virtual void Init();
	virtual void Update(float delta);
	virtual void Draw(const Renderer& renderer);
};