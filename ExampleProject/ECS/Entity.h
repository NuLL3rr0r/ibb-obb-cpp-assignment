/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Abstract base class for all entities
 */

#pragma once

#include <memory>
#include "ECS.h"
#include "Helpers/Platform.h"

class Game;
class Renderer;

class Entity
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	explicit Entity(Game* game);
	virtual ~Entity() = 0;

public:
	bool IsActive() const;

	ECS::ComponentVector& GetComponents() const;
	ECS::ComponentArray& GetComponentArray() const;
	ECS::ComponentBitSet& GetComponentBitSet() const;

	template<typename T>
	FORCEINLINE bool HasComponent() const
	{
		return GetComponentBitSet()[ECS::GetComponentTypeID<T>()];
	}

	template<typename T, typename... TArgs>
	T* AddComponent(TArgs&&... args)
	{
		std::unique_ptr<T> uptr{ std::make_unique<T>(std::forward<TArgs>(args)...) };
		uptr->SetEntity(this);
		T* c = uptr.get();
		GetComponents().emplace_back(std::move(uptr));

		GetComponentArray()[ECS::GetComponentTypeID<T>()] = c;
		GetComponentBitSet()[ECS::GetComponentTypeID<T>()] = true;

		return c;
	}

	template<typename T>
	T* GetComponent() const
	{
		auto ptr(GetComponentArray()[ECS::GetComponentTypeID<T>()]);
		return reinterpret_cast<T*>(ptr);
	}

	Game* GetGameObject() const;

public:
	virtual void Init();
	virtual void Update(float delta);
	virtual void Draw(const Renderer& renderer);
	void Destroy();
};

