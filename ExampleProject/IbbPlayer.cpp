/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The Ibb player class
 */

#include "IbbPlayer.h"
#include "Components/CollisionComponent.h"
#include "Components/MovementComponent.h"
#include "Components/SpriteComponent.h"
#include "Components/TransformComponent.h"
#include "Game.h"
#include "Helpers/loguru.h"
#include "Renderer.h"
#include "Texture.h"

using Super = Player;

struct IbbPlayer::Impl
{

};

IbbPlayer::IbbPlayer(Game* game) :
    Super(game, "content/player2.png"),
    pimpl(std::make_unique<IbbPlayer::Impl>())
{

}

IbbPlayer::~IbbPlayer() = default;

void IbbPlayer::OnInputKeyPressed(const InputComponent::Key& key)
{
    Super::OnInputKeyPressed(key);

    Player::GravityPortalState& state = GetGravityPortalState();
    if (state.CurrentSpace != state.LastSpace)
    {
        return;
    }

    MovementComponent* movement = GetMovementComponent();
    if (!movement)
    {
        return;
    }

    switch (key)
    {
    case InputComponent::Key::IBB_UP:
        if (movement->CanJump())
        {
            movement->Jump();
        }
        break;

    case InputComponent::Key::IBB_LEFT:
        movement->MoveLeft();
        break;

    case InputComponent::Key::IBB_DOWN:
        if (movement->CanJump())
        {
            movement->Jump();
        }
        break;

    case InputComponent::Key::IBB_RIGHT:
        movement->MoveRight();
        break;

    default:
        break;
    }
}

void IbbPlayer::OnInputKeyReleased(const InputComponent::Key& key)
{
    Super::OnInputKeyReleased(key);

    Player::GravityPortalState& state = GetGravityPortalState();
    if (state.CurrentSpace != state.LastSpace)
    {
        return;
    }

    MovementComponent* movement = GetMovementComponent();
    if (!movement)
    {
        return;
    }

    switch (key)
    {
    case InputComponent::Key::IBB_UP:
        break;
    case InputComponent::Key::IBB_LEFT:
        break;
    case InputComponent::Key::IBB_DOWN:
        break;
    case InputComponent::Key::IBB_RIGHT:
        break;
    default:
        break;
    }
}

void IbbPlayer::OnInputKeyRepeated(const InputComponent::Key& key)
{
    Super::OnInputKeyRepeated(key);

    Player::GravityPortalState& state = GetGravityPortalState();
    if (state.CurrentSpace != state.LastSpace)
    {
        return;
    }

    MovementComponent* movement = GetMovementComponent();
    if (!movement)
    {
        return;
    }

    switch (key)
    {
    case InputComponent::Key::IBB_UP:
        if (movement->CanJump())
        {
            movement->Jump();
        }
        break;

    case InputComponent::Key::IBB_LEFT:
        movement->MoveLeft();
        break;

    case InputComponent::Key::IBB_DOWN:
        if (movement->CanJump())
        {
            movement->Jump();
        }
        break;

    case InputComponent::Key::IBB_RIGHT:
        movement->MoveRight();
        break;

    default:
        break;
    }
}

void IbbPlayer::Init()
{
    Super::Init();

    // Set the player position at the game start
    TransformComponent* transform = GetComponent<TransformComponent>();
    if (transform)
    {
        transform->SetPosition(vmml::vec2f(100.0f, 100.0f));
    }

    // Reconstruct the player's collision as each player's height is different
    CollisionComponent* collision = GetComponent<CollisionComponent>();
    if (collision)
    {
        collision->SetCenter(vmml::vec2f(1.37f, 1.06f));
        collision->Reconstruct();
    }
}

void IbbPlayer::Update(float delta)
{
    Super::Update(delta);

    InputComponent* input = GetInputComponent();
    if (!input)
    {
        return;
    }

    /* If there is no input try to stop physics based movement (due to
    * unwanted forces from box2d) */
    if (!input->IsKeyDown(InputComponent::Key::IBB_LEFT)
        && !input->IsKeyDown(InputComponent::Key::IBB_RIGHT))
    {
        MovementComponent* movement = GetMovementComponent();
        if (!movement)
        {
            return;
        }

        if (movement->IsMoving())
        {
            movement->Stop();
        }
    }
}
