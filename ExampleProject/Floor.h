/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The game's floor class which takes care of darwing the ground
 */

#pragma once

#include <memory>
#include "VMMLib/vector2.h"

class Game;
class Renderer;

class Floor
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	Floor(Game* game);
	virtual ~Floor();

public:
	float GetUpperSpaceYCoordinate() const;
	float GetLowerSpaceYCoordinate() const;

	/* GP stands for Gravity Portal */
	vmml::vec2f GetGPUpperLeftCoordinates() const;
	vmml::vec2f GetGPLowerLeftCoordinates() const;
	vmml::vec2f GetGPUpperRightCoordinates() const;
	vmml::vec2f GetGPLowerRightCoordinates() const;

public:
	void Init();
	void Update(float delta);
	void Draw(const Renderer& renderer);
};