#pragma once

#include <iostream>

#include <SDL.h>
#include <SDL_image.h>
#include "vmmlib.h"

class Renderer;

class Texture
{
private:
	Texture();

public:
	Texture(const Renderer& renderer, const std::string& filepath);

	~Texture();

	void Destroy();

	bool IsEmpty();

	SDL_Texture* GetTexture();

	void SetAlphaMod(int alpha);
	void SetColorMod(Vector4ub color);

	int GetAlphaMod();
	Vector4ub GetColorMod();
	Vector2i GetSize();
	Uint32 GetFormat();
	int GetAccess();

private:
	SDL_Texture* texture;
};