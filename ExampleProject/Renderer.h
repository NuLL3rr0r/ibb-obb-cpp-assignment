#pragma once

#include "Helpers/Integer.h"
#include "Window.h"
#include <SDL.h>
#include "vmmlib.h"

class Texture;

class Renderer
{
public:
	Renderer(Window& window);
	virtual ~Renderer();

	void Clear(byte r, byte g, byte b);

	void Present();

	void Draw(Texture* texture, const Vector2f& position) const;

	Window& GetWindow() const;
	SDL_Renderer* GetRenderer() const;

private:
	SDL_Renderer* sdlRenderer;
	Window& window;
};