/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's transform component
 */

#include "TransformComponent.h"
#include "ECS/Entity.h"

using Super = Component;

struct TransformComponent::Impl
{
    vmml::vec2f Position;
};

TransformComponent::TransformComponent() :
    Super(),
    pimpl(std::make_unique<TransformComponent::Impl>())
{
    pimpl->Position = vmml::vec2f(0.0f, 0.0f);
}

TransformComponent::TransformComponent(float x, float y) :
    Super(),
    pimpl(std::make_unique<TransformComponent::Impl>())
{
    pimpl->Position.x = x;
    pimpl->Position.y = y;
}

TransformComponent::~TransformComponent() = default;

const vmml::vec2f& TransformComponent::GetPosition() const
{
    return pimpl->Position;
}

void TransformComponent::SetPosition(const vmml::vec2f& position)
{
    pimpl->Position = position;

    const Entity* entity = this->GetEntity();
    if (!entity)
    {
        return;
    }
}

void TransformComponent::Add(const vmml::vec2f& vec)
{
    pimpl->Position += vec;
}