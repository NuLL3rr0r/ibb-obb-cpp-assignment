/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's collision component
 */

#include "CollisionComponent.h"
#include "box2d/box2d.h"
#include "box2d/b2_math.h"
#include "Components/SpriteComponent.h"
#include "Components/TransformComponent.h"
#include "ECS/Entity.h"
#include "Game.h"
#include "PhysicsManager.h"
#include "Texture.h"
#include "VMMLib/vector2.h"

using Super = Component;

struct CollisionComponent::Impl
{
	vmml::vec2f Center;

	void DestroyBody(Entity* entity, b2Body* body)
	{
		if (!entity)
		{
			return;
		}

		Game* game = entity->GetGameObject();
		if (!game)
		{
			return;
		}

		if (!body)
		{
			return;
		}

		PhysicsManager& physicsManager = game->GetPhysicsManager();
		b2World& world = physicsManager.GetWorld();

		world.DestroyBody(body);
	}

	void CreateBody(Entity* entity)
	{
		if (!entity)
		{
			return;
		}

		Game* game = entity->GetGameObject();
		if (!game)
		{
			return;
		}

		const SpriteComponent* sprite = entity->GetComponent<SpriteComponent>();
		if (!sprite)
		{
			return;
		}

		Texture* texture = sprite->GetTexture();
		if (!texture)
		{
			return;
		}

		const vmml::vec2f extent = vmml::vec2f(
			(float)texture->GetSize().x,
			(float)texture->GetSize().y);

		if (extent == vmml::vec2f::ZERO)
		{
			return;
		}

		const TransformComponent* transform = entity->GetComponent<TransformComponent>();
		if (!transform)
		{
			return;
		}

		const vmml::vec2f position = transform->GetPosition();

		PhysicsManager& physicsManager = game->GetPhysicsManager();
		physicsManager.CreateBox(entity, extent, position, Center);
	}
};

CollisionComponent::CollisionComponent(const vmml::vec2f& center) :
    Super(),
    pimpl(std::make_unique<CollisionComponent::Impl>())
{
	pimpl->Center = center;
}

CollisionComponent::~CollisionComponent() = default;

b2Body* CollisionComponent::GetBody(Entity* entity) const
{
	if (!entity)
	{
		return nullptr;
	}

	Game* game = entity->GetGameObject();
	if (!game)
	{
		return nullptr;
	}

	PhysicsManager& physicsManager = game->GetPhysicsManager();
	b2World& world = physicsManager.GetWorld();
	Entity* attachedObject = nullptr;

	/* Query box2d's world and find and return the underlying box2d collision
	* for this component*/
	for (b2Body* bodyIterator = world.GetBodyList();
		bodyIterator != nullptr;
		bodyIterator = bodyIterator->GetNext())
	{
		if (bodyIterator->GetType() == b2_dynamicBody)
		{
			attachedObject = static_cast<Entity*>(bodyIterator->GetUserData());
			if (attachedObject == entity)
			{
				return bodyIterator;
			}
		}
	}

	return nullptr;
}

void CollisionComponent::SetCenter(const vmml::vec2f& center)
{
	pimpl->Center = center;
}

vmml::vec2f CollisionComponent::GetLinearVelocity() const
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return vmml::vec2f(0.0f, 0.0f);
	}

	b2Body* body = GetBody(entity);
	if (!body)
	{
		return vmml::vec2f(0.0f, 0.0f);
	}

	b2Vec2 velocity(body->GetLinearVelocity());
	return vmml::vec2f(velocity.x, velocity.y);
}

void CollisionComponent::SetLinearVelocity(const vmml::vec2f& velocity)
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	b2Body* body = GetBody(entity);
	if (!body)
	{
		return;
	}

	body->SetLinearVelocity(b2Vec2(velocity.x, velocity.y));
}

void CollisionComponent::Init()
{
    Super::Init();

	Reconstruct();
}

void CollisionComponent::Update(float delta)
{
	Super::Update(delta);
}

void CollisionComponent::Reconstruct()
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	b2Body* body = GetBody(entity);
	pimpl->DestroyBody(entity, body);
	pimpl->CreateBody(entity);
}

void CollisionComponent::ApplyForce(const vmml::vec2f& force)
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	b2Body* body = GetBody(entity);
	if (!body)
	{
		return;
	}

	b2Vec2 vec(force.x, force.y);
	body->ApplyForceToCenter(vec, true);
}

void CollisionComponent::ApplyLinearImpulse(const vmml::vec2f& impulse)
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	b2Body* body = GetBody(entity);
	if (!body)
	{
		return;
	}

	const float mass = body->GetMass();
	b2Vec2 vec(mass * impulse.x, mass * impulse.y);
	body->ApplyLinearImpulseToCenter(vec, true);
}
