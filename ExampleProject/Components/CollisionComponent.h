/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's collision component
 */

#pragma once

#include <memory>
#include "ECS/Component.h"
#include "VMMLib/vector2.h"

class b2Body;

class CollisionComponent : public Component
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	explicit CollisionComponent(
		const vmml::vec2f& center = vmml::vec2f(1.0f, 1.0f));
	virtual ~CollisionComponent();

public:
	/// <summary>
	/// Returns the underlying box2d collision
	/// </summary>
	/// <param name="entity">The owning entity</param>
	/// <returns>The underlying box2d collision</returns>
	b2Body* GetBody(Entity* entity) const;

	/// <summary>
	/// Adjusts the collision center used to apply force or linear impulse
	/// </summary>
	/// <param name="center">The physics center (between -1.0f and 1.0f)</param>
	void SetCenter(const vmml::vec2f& center);

	vmml::vec2f GetLinearVelocity() const;
	void SetLinearVelocity(const vmml::vec2f& velocity);

public:
	void Init() override;
	void Update(float delta) override;

	/// <summary>
	/// Reconstructs the collision component, it should be called e.g. after
	/// resetting the center using SetCenter
	/// </summary>
	void Reconstruct();

public:
	void ApplyForce(const vmml::vec2f& force);
	void ApplyLinearImpulse(const vmml::vec2f& impulse);
};