/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's input component
 */

#include "InputComponent.h"
#include <unordered_map>
#include <SDL_events.h>
#include "ECS/Entity.h"
#include "Game.h"
#include "Helpers/loguru.h"
#include "Input.h"

using Super = Component;

struct InputComponent::Impl
{
	std::unordered_map<int, InputComponent::Key> KeyMap;
	InputComponent::KeyStateChangeCallback KeyPressed;
	InputComponent::KeyStateChangeCallback KeyReleased;
	InputComponent::KeyStateChangeCallback KeyRepeated;
};

InputComponent::InputComponent(
	KeyStateChangeCallback keyPressedCallback,
	KeyStateChangeCallback keyReleasedCallback,
	KeyStateChangeCallback keyRepeatedCallback) :
    Super(),
    pimpl(std::make_unique<InputComponent::Impl>())
{
	pimpl->KeyMap = {
		{SDLK_UP, Key::IBB_UP},
		{SDLK_LEFT, Key::IBB_LEFT},
		{SDLK_DOWN, Key::IBB_DOWN},
		{SDLK_RIGHT, Key::IBB_RIGHT},
		{SDLK_w, Key::OBB_UP},
		{SDLK_a, Key::OBB_LEFT},
		{SDLK_s, Key::OBB_DOWN},
		{SDLK_d, Key::OBB_RIGHT},
	};

	pimpl->KeyPressed = keyPressedCallback;
	pimpl->KeyReleased = keyReleasedCallback;
	pimpl->KeyRepeated = keyRepeatedCallback;
}

InputComponent::~InputComponent() = default;

void InputComponent::Update(float delta)
{
	Super::Update(delta);

	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	Game* game = entity->GetGameObject();
	if (!game)
	{
		return;
	}

	Input& input = game->GetInput();
	for (const auto& kv : pimpl->KeyMap)
	{
		if (input.keyboard.OnKeyPress(kv.first))
		{
			if (pimpl->KeyPressed)
			{
				pimpl->KeyPressed(kv.second);
			}
		}
		if (input.keyboard.OnKeyRelease(kv.first))
		{
			if (pimpl->KeyReleased)
			{
				pimpl->KeyReleased(kv.second);
			}
		}
		if (input.keyboard.IsKeyPressed(kv.first))
		{
			if (pimpl->KeyRepeated)
			{
				pimpl->KeyRepeated(kv.second);
			}
		}
	}
}

bool InputComponent::IsKeyDown(const Key& key) const
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return false;
	}

	Game* game = entity->GetGameObject();
	if (!game)
	{
		return false;
	}

	Input& input = game->GetInput();
	for (const auto& kv : pimpl->KeyMap)
	{
		if (input.keyboard.OnKeyPress(kv.first)
			|| input.keyboard.IsKeyPressed(kv.first))
		{
			return true;
		}
	}

	return false;
}