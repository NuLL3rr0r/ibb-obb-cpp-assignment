/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's input component
 */

#pragma once

#include <functional>
#include <memory>
#include "ECS/Component.h"
#include "VMMLib/vector2.h"

class InputComponent : public Component
{
public:
	enum class Key : uint8_t
	{
		IBB_UP,
		IBB_LEFT,
		IBB_DOWN,
		IBB_RIGHT,
		OBB_UP,
		OBB_LEFT,
		OBB_DOWN,
		OBB_RIGHT,
	};
	using KeyStateChangeCallback = std::function<void(const Key&)>;

private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	InputComponent(KeyStateChangeCallback keyPressedCallback,
		KeyStateChangeCallback keyReleasedCallback,
		KeyStateChangeCallback keyRepeatedCallback);
	virtual ~InputComponent();

public:
	void Update(float delta) override;

	bool IsKeyDown(const Key& key) const;
};

