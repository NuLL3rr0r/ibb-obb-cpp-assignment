/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's movement component
 */

#pragma once

#include <functional>
#include <memory>
#include "ECS/Component.h"

class MovementComponent : public Component
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	MovementComponent();
	virtual ~MovementComponent();

public:
	void Update(float delta) override;

	bool IsInAir() const;
	bool CanJump() const;
	void Jump();

	bool IsMoving() const;
	void MoveLeft();
	void MoveRight();
	void Stop();
};

