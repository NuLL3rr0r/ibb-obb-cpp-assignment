/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's sprite component
 */

#include "SpriteComponent.h"
#include "Components/TransformComponent.h"
#include "Game.h"
#include "Player.h"
#include "Renderer.h"
#include "TextureManager.h"

using Super = Component;

struct SpriteComponent::Impl
{
    std::string FilePath;
    Texture* Texture;
};

SpriteComponent::SpriteComponent(const std::string& filepath) :
    Super(),
    pimpl(std::make_unique<SpriteComponent::Impl>())
{
    pimpl->FilePath = filepath;
}

SpriteComponent::~SpriteComponent() = default;

Texture* SpriteComponent::GetTexture() const
{
    return pimpl->Texture;
}

void SpriteComponent::Init()
{
    Super::Init();

    const Entity* entity = GetEntity();
    if (!entity)
    {
        return;
    }

    Game* game = entity->GetGameObject();
    if (!game)
    {
        return;
    }

    TextureManager& textureManager = game->GetTextureManager();
    pimpl->Texture = textureManager.AddTexture(pimpl->FilePath);
}

void SpriteComponent::Draw(const Renderer& renderer)
{
    Super::Draw(renderer);

    Entity* entity = GetEntity();
    if (pimpl->Texture && entity && entity->HasComponent<TransformComponent>())
    {
        const TransformComponent* transform =
            entity->GetComponent<TransformComponent>();
        if (transform)
        {
            renderer.Draw(pimpl->Texture, transform->GetPosition());
        }
    }
}