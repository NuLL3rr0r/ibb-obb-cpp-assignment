/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's sprite component
 */

#pragma once

#include <memory>
#include <string>
#include "ECS/Component.h"

class Texture;

class SpriteComponent : public Component
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	explicit SpriteComponent(const std::string& filepath);
	virtual ~SpriteComponent();

public:
	Texture* GetTexture() const;

public:
	void Init() override;
	void Draw(const Renderer& renderer) override;
};

