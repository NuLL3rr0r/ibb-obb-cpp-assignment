/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's transform component
 */

#pragma once

#include <memory>
#include "ECS/Component.h"
#include "VMMLib/vector2.h"

class TransformComponent : public Component
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	TransformComponent();
	TransformComponent(float x, float y);
	virtual ~TransformComponent();

public:
	const vmml::vec2f& GetPosition() const;
	void SetPosition(const vmml::vec2f& position);

	void Add(const vmml::vec2f& position);
};

