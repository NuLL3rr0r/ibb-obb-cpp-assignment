/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The player's movement component
 */

#include "box2d/box2d.h"
#include "Components/CollisionComponent.h"
#include "Components/MovementComponent.h"
#include "ECS/Entity.h"
#include "Helpers/Float.h"
#include "Helpers/loguru.h"
#include "VMMLib/vector2.h"

using Super = Component;

struct MovementComponent::Impl
{
	const float AirSpeed = 5.0f;
	const float GroundSpeed = 90.0f;
	const float JumpSpeed = 10.0f;

	vmml::vec2f TargetDirection;
};

MovementComponent::MovementComponent() :
	Super(),
	pimpl(std::make_unique<MovementComponent::Impl>())
{

}

MovementComponent::~MovementComponent() = default;

void MovementComponent::Update(float delta)
{
	Super::Update(delta);
}

bool MovementComponent::IsInAir() const
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return false;
	}

	CollisionComponent* collision = entity->GetComponent<CollisionComponent>();
	if (!collision)
	{
		return false;
	}

	vmml::vec2f velocity(collision->GetLinearVelocity());
	if (Float::IsNearlyZero(velocity.y))
	{
		return false;
	}

	return true;
}

bool MovementComponent::CanJump() const
{
	return !IsInAir();
}

void MovementComponent::Jump()
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	CollisionComponent* collision = entity->GetComponent<CollisionComponent>();
	if (!collision)
	{
		return;
	}

	if (!CanJump())
	{
		return;
	}

	b2Body* body = collision->GetBody(entity);
	if (!body)
	{
		return;
	}

	const float gravityScale = body->GetGravityScale();

	/* Determine if player is in the upper screen or the lower screen
	* and adjust the jump direction based on that */
	const float speed = Float::IsNearlyEqual(gravityScale, 1.0f)
		? -pimpl->JumpSpeed
		: pimpl->JumpSpeed;

	collision->ApplyLinearImpulse(vmml::vec2f(0.0f, speed));
}

bool MovementComponent::IsMoving() const
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return false;
	}

	CollisionComponent* collision = entity->GetComponent<CollisionComponent>();
	if (!collision)
	{
		return false;
	}

	vmml::vec2f velocity(collision->GetLinearVelocity());
	if (Float::IsNearlyZero(velocity.x))
	{
		return false;
	}

	return true;
}

void MovementComponent::MoveLeft()
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	CollisionComponent* collision = entity->GetComponent<CollisionComponent>();
	if (!collision)
	{
		return;
	}

	if (!IsInAir())
	{
		collision->ApplyForce(vmml::vec2f(-pimpl->GroundSpeed, 0.0f));
	}
	else
	{
		collision->ApplyForce(vmml::vec2f(-pimpl->AirSpeed, 0.0f));
	}
}

void MovementComponent::MoveRight()
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	CollisionComponent* collision = entity->GetComponent<CollisionComponent>();
	if (!collision)
	{
		return;
	}

	if (!IsInAir())
	{
		collision->ApplyForce(vmml::vec2f(pimpl->GroundSpeed, 0.0f));
	}
	else
	{
		collision->ApplyForce(vmml::vec2f(pimpl->AirSpeed, 0.0f));
	}
}

void MovementComponent::Stop()
{
	Entity* entity = GetEntity();
	if (!entity)
	{
		return;
	}

	CollisionComponent* collision = entity->GetComponent<CollisionComponent>();
	if (!collision)
	{
		return;
	}

	vmml::vec2f velocity = collision->GetLinearVelocity();
	velocity.x = 0.0f;
	collision->SetLinearVelocity(velocity);
}