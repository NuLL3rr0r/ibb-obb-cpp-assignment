/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The game's floor class which takes care of darwing the ground
 */

#include "Floor.h"
#include "Game.h"
#include "PhysicsManager.h"
#include "Renderer.h"
#include "Texture.h"
#include "TextureManager.h"
#include "VMMLib/vector2.h"

struct Floor::Impl
{
    Game* Game;
    Texture* Texture;

	vmml::vec2f LeftFloorSize;
	vmml::vec2f LeftFloorPosition;
	vmml::vec2f RightFloorSize;
	vmml::vec2f RightFloorPosition;
	vmml::vec2f GravityPortalSize;
	vmml::vec2f GravityPortalPosition;
};

Floor::Floor(Game* game) :
    pimpl(std::make_unique<Floor::Impl>())
{
    pimpl->Game = game;
}

Floor::~Floor() = default;

float Floor::GetUpperSpaceYCoordinate() const
{
	return static_cast<float>(pimpl->GravityPortalPosition.y);
}

float Floor::GetLowerSpaceYCoordinate() const
{
	return static_cast<float>(
		pimpl->GravityPortalPosition.y + pimpl->GravityPortalSize.y);
}

vmml::vec2f Floor::GetGPUpperLeftCoordinates() const
{
	return vmml::vec2f(
		pimpl->GravityPortalPosition.x, pimpl->GravityPortalPosition.y);
}

vmml::vec2f Floor::GetGPLowerLeftCoordinates() const
{
	return vmml::vec2f(
		pimpl->GravityPortalPosition.x,
		pimpl->GravityPortalPosition.y + pimpl->GravityPortalSize.y);
}

vmml::vec2f Floor::GetGPUpperRightCoordinates() const
{
	return vmml::vec2f(
		pimpl->GravityPortalPosition.x + pimpl->GravityPortalSize.x,
		pimpl->GravityPortalPosition.y);
}

vmml::vec2f Floor::GetGPLowerRightCoordinates() const
{
	return vmml::vec2f(
		pimpl->GravityPortalPosition.x + pimpl->GravityPortalSize.x,
		pimpl->GravityPortalPosition.y + pimpl->GravityPortalSize.y);
}

void Floor::Init()
{
    pimpl->Texture = pimpl->Game->GetTextureManager().AddTexture("content/floor.png");

	const float windowWidth = static_cast<float>(pimpl->Game->GetWindow().GetSize().x);
	const float windowHeight = static_cast<float>(pimpl->Game->GetWindow().GetSize().y);

	const float floorWidth = windowWidth / 2.5f;
	const float floorHeight = 16.0f;

	pimpl->LeftFloorPosition =
		vmml::vec2f(0, (windowHeight - floorHeight) / 2.0f);
	pimpl->LeftFloorSize = vmml::vec2f(floorWidth, floorHeight);

	pimpl->RightFloorPosition =
		vmml::vec2f(windowWidth - floorWidth, pimpl->LeftFloorPosition.y);
	pimpl->RightFloorSize = vmml::vec2f(floorWidth, floorHeight);

	pimpl->GravityPortalSize =
		vmml::vec2f(windowWidth - (pimpl->LeftFloorSize.x + pimpl->RightFloorSize.x),
		floorHeight);
	pimpl->GravityPortalPosition =
		vmml::vec2f(pimpl->LeftFloorPosition.x + pimpl->LeftFloorSize.x,
		pimpl->LeftFloorPosition.y);

	pimpl->Game->GetPhysicsManager().CreateGround(
		pimpl->LeftFloorSize, pimpl->LeftFloorPosition);

	pimpl->Game->GetPhysicsManager().CreateGround(
		pimpl->RightFloorSize, pimpl->RightFloorPosition);

	pimpl->Game->GetPhysicsManager().CreateGround(
		pimpl->GravityPortalSize, pimpl->GravityPortalPosition, true);
}

void Floor::Update(float delta)
{

}

void Floor::Draw(const Renderer& renderer)
{
	SDL_Rect leftFloorRect;
	leftFloorRect.x = static_cast<int>(pimpl->LeftFloorPosition.x);
	leftFloorRect.y = static_cast<int>(pimpl->LeftFloorPosition.y);
	leftFloorRect.w = static_cast<int>(pimpl->LeftFloorSize.x);
	leftFloorRect.h = static_cast<int>(pimpl->LeftFloorSize.y);
	SDL_RenderCopy(renderer.GetRenderer(), pimpl->Texture->GetTexture(),
		NULL, &leftFloorRect);
	
	SDL_Rect rightFloorRect;
	rightFloorRect.x = static_cast<int>(pimpl->RightFloorPosition.x);
	rightFloorRect.y = static_cast<int>(pimpl->RightFloorPosition.y);
	rightFloorRect.w = static_cast<int>(pimpl->RightFloorSize.x);
	rightFloorRect.h = static_cast<int>(pimpl->RightFloorSize.y);
	SDL_RenderCopy(renderer.GetRenderer(), pimpl->Texture->GetTexture(),
		NULL, &rightFloorRect);
}