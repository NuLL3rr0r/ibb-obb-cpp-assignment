#include <stdio.h>

#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <SDL_image.h>

#include "Window.h"
#include "Renderer.h"
#include "Helpers\WindowsTimer.h"
#include "Game.h"
#include "TextureManager.h"
#include "Input.h"

#include "Helpers/loguru.h"

int main(int argc, char* argv[])
{
	loguru::init(argc, argv);

	printf("Hello World \n");

	Window window(1024, 768);
	Renderer renderer(window);
	TextureManager textureManager(renderer);
	Input input;

	Game game(textureManager, input, window);
	game.Init();

	//Main loop flag
	bool quit = false;

	WindowsTimer timer;
	float previousFrameTimeInSeconds = 0.0f;

	//While application is running
	while (!quit)
	{
		//Handle events on queue
		while (input.PollEvent())
		{
			//User requests quit
			if (input.sdlEvent.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (input.keyboard.IsKeyPressed(SDLK_ESCAPE))
			{
				quit = true;
			}
		}

		timer.Start();

		game.Update(previousFrameTimeInSeconds);

		renderer.Clear(101, 156, 239);

		game.Draw(renderer);

		renderer.Present();
		timer.Stop();

		previousFrameTimeInSeconds = timer.GetTimeInSeconds();
	}

	//Destroy window
	SDL_DestroyWindow(window.GetWindow());

	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}
