/**
 * @file
 *
 * @section DESCRIPTION
 *
 * The main game module
 */

#pragma once

#include <memory>

class Floor;
class Input;
class PhysicsManager;
class Renderer;
class Texture;
class TextureManager;
class Window;

class Game
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	Game(TextureManager& textureManager, Input& input, Window& window);
	virtual ~Game();

public:
	TextureManager& GetTextureManager() const;
	Input& GetInput() const;
	Window& GetWindow() const;

	PhysicsManager& GetPhysicsManager() const;
	Floor& GetFloor() const;

public:
	void Init();
	void Update(float delta);
	void Draw(const Renderer& renderer);
};