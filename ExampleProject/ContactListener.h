/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * A collision listner/detector which is being used in conjunction with the
 * physics manager
 */

#pragma once

#include <memory>
#include "box2d/box2d.h"

class PhysicsManager;

class ContactListener : public b2ContactListener
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	ContactListener(PhysicsManager* physicsManager);
	virtual ~ContactListener();

public:
	virtual void BeginContact(b2Contact* contact) override;
	virtual void EndContact(b2Contact* contact) override;
};