/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Abstract base class for all players in the game
 */

#pragma once

#include <memory>
#include <string>
#include "VMMLib/vector2.h"
#include "Components/InputComponent.h"
#include "ECS/Entity.h"
#include "Helpers/Integer.h"
#include "Helpers/loguru.h"
#include "Helpers/Platform.h"

class Renderer;
class CollisionComponent;
class MovementComponent;
class SpriteComponent;
class TransformComponent;

class Player : public Entity
{
public:
	struct GravityPortalState
	{
		enum class Space : uint8
		{
			Upper,
			Lower,
		};

		float EnteringXVelocity;
		vmml::vec2f LandingSpot;
		Space LastSpace;
		Space CurrentSpace;
	};

private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	Player(Game* game, const std::string& spriteFilePath);
	virtual ~Player() = 0;

protected:
	virtual void OnInputKeyPressed(const InputComponent::Key& key);
	virtual void OnInputKeyReleased(const InputComponent::Key& key);
	virtual void OnInputKeyRepeated(const InputComponent::Key& key);

public:
	GravityPortalState& GetGravityPortalState();

protected:
	FORCEINLINE CollisionComponent* GetPhysicsComponent() const
	{
		CHECK_F(HasComponent<CollisionComponent>(), "Collision component is missing!");
		return GetComponent<CollisionComponent>();
	}

	FORCEINLINE InputComponent* GetInputComponent() const
	{
		CHECK_F(HasComponent<InputComponent>(), "Input component is missing!");
		return GetComponent<InputComponent>();
	}

	FORCEINLINE MovementComponent* GetMovementComponent() const
	{
		CHECK_F(HasComponent<MovementComponent>(), "Movement component is missing!");
		return GetComponent<MovementComponent>();
	}

	FORCEINLINE SpriteComponent* GetSpriteComponent() const
	{
		CHECK_F(HasComponent<SpriteComponent>(), "Sprite component is missing!");
		return GetComponent<SpriteComponent>();
	}

	FORCEINLINE TransformComponent* GetTransformComponent() const
	{
		CHECK_F(HasComponent<TransformComponent>(), "Transform component is missing!");
		return GetComponent<TransformComponent>();
	}

public:
	void Init() override;
	void Update(float delta) override;
};
