#include "Texture.h"
#include "Renderer.h"

Texture::Texture() :
	texture(NULL)
{

}

Texture::Texture(const Renderer& renderer, const std::string& filepath) :
	texture(NULL)
{
	texture = IMG_LoadTexture(renderer.GetRenderer(), filepath.c_str());

	if (IsEmpty())
	{
		printf("Couldn't load Texture! Filepath: %s \n", filepath.c_str());
	}
	else
	{
		printf("Texture loaded: %s \n", filepath.c_str());
	}
}

Texture::~Texture()
{
	Destroy();
}

void Texture::Destroy()
{
	if (!IsEmpty())
	{
		SDL_DestroyTexture(texture);
	}

	texture = NULL;
}

bool Texture::IsEmpty()
{
	return ((texture == NULL) || (texture == nullptr));
}

SDL_Texture* Texture::GetTexture()
{
	if (!IsEmpty())
	{
		return texture;
	}
	else
	{
		return NULL;
	}
}

void Texture::SetAlphaMod(int alpha)
{
	if (SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND) == -1)
	{
		printf("Couldn't set blend mode!");
		assert(false);
	}

	if (SDL_SetTextureAlphaMod(texture, alpha) == -1)
	{
		printf("Couldn't set alpha mod!");
		assert(false);
	}
}

void Texture::SetColorMod(Vector4ub color)
{
	SDL_SetTextureColorMod(texture, color.r, color.g, color.b);
}

int Texture::GetAlphaMod()
{
	Uint8 alpha;

	if (SDL_GetTextureAlphaMod(texture, &alpha) == -1)
	{
		printf("Couldn't get alpha mod");
		assert(false);
	}

	return alpha;
}

Vector4ub Texture::GetColorMod()
{
	SDL_Color temp;
	SDL_GetTextureColorMod(texture, &temp.r, &temp.g, &temp.b);
	return Vector4ub(temp.r, temp.g, temp.b, 255);
}

Vector2i Texture::GetSize()
{
	int w, h;
	SDL_QueryTexture(texture, NULL, NULL, &w, &h);
	return Vector2i(w, h);
}

Uint32 Texture::GetFormat()
{
	Uint32 format;
	SDL_QueryTexture(texture, &format, NULL, NULL, NULL);
	return format;
}

int Texture::GetAccess()
{
	int access;
	SDL_QueryTexture(texture, NULL, &access, NULL, NULL);
	return access;
}