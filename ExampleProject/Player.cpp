/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Abstract base class for all players in the game
 */

#include "Player.h"
#include "Game.h"
#include "Components/CollisionComponent.h"
#include "Components/InputComponent.h"
#include "Components/MovementComponent.h"
#include "Components/SpriteComponent.h"
#include "Components/TransformComponent.h"
#include "Renderer.h"

using Super = Entity;

struct Player::Impl
{
    std::string SpriteFilePath;

    Player::GravityPortalState GravityPortalState;
};

Player::Player(Game* game, const std::string& spriteFilePath) :
    Super(game),
    pimpl(std::make_unique<Player::Impl>())
{
    pimpl->SpriteFilePath = spriteFilePath;
}

Player::~Player() = default;

void Player::OnInputKeyPressed(const InputComponent::Key& key)
{

}

void Player::OnInputKeyReleased(const InputComponent::Key& key)
{

}

void Player::OnInputKeyRepeated(const InputComponent::Key& key)
{

}

Player::GravityPortalState& Player::GetGravityPortalState()
{
    return pimpl->GravityPortalState;
}

void Player::Init()
{
    AddComponent<InputComponent>(
        std::bind(&Player::OnInputKeyPressed, this, std::placeholders::_1),
        std::bind(&Player::OnInputKeyReleased, this, std::placeholders::_1),
        std::bind(&Player::OnInputKeyRepeated, this, std::placeholders::_1));
    AddComponent<MovementComponent>();
    AddComponent<SpriteComponent>(pimpl->SpriteFilePath);
    AddComponent<TransformComponent>();

    // Collision component should be initialized after all components
    // otherwise, Reconstruct method should be called
    AddComponent<CollisionComponent>();

    // Super should be initialized after all components
    Super::Init();

    pimpl->GravityPortalState.EnteringXVelocity = 0.0f;
    pimpl->GravityPortalState.LastSpace = GravityPortalState::Space::Upper;
    pimpl->GravityPortalState.CurrentSpace = GravityPortalState::Space::Upper;
}

void Player::Update(float delta)
{
    Super::Update(delta);
}