/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * A generic physics manager which takes care of all physics stuff in the game
 */

#pragma once

#include <memory>
#include <VMMLib/vector2.h>

class b2World;
class Game;
class Player;
class Renderer;

class PhysicsManager
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	PhysicsManager(Game* game);
	virtual ~PhysicsManager();

public:
	void OnPlayerEntersGravityPortal(Player* player);
	void OnPlayerLeavesGravityPortal(Player* player);

public:
	b2World& GetWorld() const;

public:
	void Init();
	void Update(float delta);

	/// <summary>
	/// Used for debug drawing all collisions; Comment the content if you don't
	/// need debugging the collision
	/// </summary>
	/// <param name="renderer">The main renderer passed from the game object</param>
	void DebugDraw(const Renderer& renderer);

	/// <summary>
	/// Create box2d collision for static floors and gravity portals
	/// </summary>
	/// <param name="size">Size of the collision</param>
	/// <param name="position">Location of the collision in the world</param>
	/// <param name="bIsGravityPortal">Wheter it is a gravity portal or no (player falls through)</param>
	void CreateGround(const vmml::vec2f& size, const vmml::vec2f& position,
		bool bIsGravityPortal = false);

	/// <summary>
	/// Create box2d collision for dynamic objects (e.g. the players)
	/// </summary>
	/// <param name="attachedObject">The attached object (e.g. the player)</param>
	/// <param name="size">The size of this collision object</param>
	/// <param name="position">The world location of this collision object</param>
	/// <param name="center">The center of this collision (between -1.0f, 1.0f)</param>
	/// <param name="density">The density, usually in kg/m^2</param>
	/// <param name="friction">The friction coefficient, usually in the range [0,1]</param>
	void CreateBox(
		void* attachedObject,
		const vmml::vec2f& size, const vmml::vec2f& position,
		const vmml::vec2f& center = vmml::vec2f(0.0f, 0.0f),
		float density = 1.0f, float friction = 0.0f);
};