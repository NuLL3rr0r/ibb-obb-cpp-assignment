/**
 * @file
 *
 * @section DESCRIPTION
 *
 * The main game module
 */

#include "Game.h"
#include <cstdio>
#include "ECS/EntityManager.h"
#include "Floor.h"
#include "IbbPlayer.h"
#include "Input.h"
#include "ObbPlayer.h"
#include "PhysicsManager.h"
#include "Renderer.h"
#include "Texture.h"
#include "TextureManager.h"
#include "Window.h"

struct Game::Impl
{
	TextureManager& TextureManagerRef;
	Input& InputRef;
	Window& WindowRef;

	std::unique_ptr<EntityManager> EntityManager;
	std::unique_ptr<Floor> Floor;
	std::unique_ptr<PhysicsManager> PhysicsManager;

	IbbPlayer* Ibb;
	ObbPlayer* Obb;
	
	Impl(TextureManager& textureManager, Input& input, Window& window) :
		TextureManagerRef(textureManager),
		InputRef(input),
		WindowRef(window)
	{

	}
};

Game::Game(TextureManager& textureManager, Input& input, Window& window) :
	pimpl(std::make_unique<Game::Impl>(textureManager, input, window))
{

}

Game::~Game() = default;

TextureManager& Game::GetTextureManager() const
{
	return pimpl->TextureManagerRef;
}

PhysicsManager& Game::GetPhysicsManager() const
{
	return *pimpl->PhysicsManager;
}

Floor& Game::GetFloor() const
{
	return *pimpl->Floor;
}

Input& Game::GetInput() const
{
	return pimpl->InputRef;
}

Window& Game::GetWindow() const
{
	return pimpl->WindowRef;
}

void Game::Init()
{
	pimpl->PhysicsManager = std::make_unique<PhysicsManager>(this);
	pimpl->PhysicsManager->Init();

	pimpl->Floor = std::make_unique<Floor>(this);
	pimpl->Floor->Init();

	pimpl->EntityManager = std::make_unique<EntityManager>();
	pimpl->Ibb = pimpl->EntityManager->AddEntity<IbbPlayer>(this);
	pimpl->Obb = pimpl->EntityManager->AddEntity<ObbPlayer>(this);
	// Initialize all entities
	pimpl->EntityManager->Init();
}

void Game::Update(float delta)
{
	// Update all entities
	pimpl->EntityManager->Update(delta);

	pimpl->Floor->Update(delta);

	pimpl->PhysicsManager->Update(delta);
}

void Game::Draw(const Renderer& renderer)
{
	pimpl->Floor->Draw(renderer);

	// Draw all entities
	pimpl->EntityManager->Draw(renderer);

	pimpl->PhysicsManager->DebugDraw(renderer);
}