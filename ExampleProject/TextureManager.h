#pragma  once

#include <SDL.h>
#include <SDL_image.h>

#include <vector>

#include "Renderer.h"

class Texture;

class TextureManager
{
public:
	TextureManager(Renderer& renderer);
	~TextureManager();

	Texture* AddTexture(const std::string& filepath);
	void RemoveTexture(Texture* texture);

	Texture* GetTexture(unsigned int key);

	void Clear();

private:
	std::vector<Texture*> textures;
	Renderer& renderer;
};