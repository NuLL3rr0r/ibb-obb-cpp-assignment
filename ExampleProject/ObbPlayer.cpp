/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The Obb player class
 */

#include "ObbPlayer.h"
#include "Components/CollisionComponent.h"
#include "Components/MovementComponent.h"
#include "Components/TransformComponent.h"
#include "Game.h"
#include "Helpers/loguru.h"
#include "Renderer.h"

using Super = Player;

struct ObbPlayer::Impl
{

};

ObbPlayer::ObbPlayer(Game* game) :
    Super(game, "content/player.png"),
    pimpl(std::make_unique<ObbPlayer::Impl>())
{

}

ObbPlayer::~ObbPlayer() = default;

void ObbPlayer::OnInputKeyPressed(const InputComponent::Key& key)
{
    Super::OnInputKeyPressed(key);

    Player::GravityPortalState& state = GetGravityPortalState();
    if (state.CurrentSpace != state.LastSpace)
    {
        return;
    }

    MovementComponent* movement = GetMovementComponent();
    if (!movement)
    {
        return;
    }

    switch (key)
    {
    case InputComponent::Key::OBB_UP:
        if (movement->CanJump())
        {
            movement->Jump();
        }
        break;

    case InputComponent::Key::OBB_LEFT:
        movement->MoveLeft();
        break;

    case InputComponent::Key::OBB_DOWN:
        if (movement->CanJump())
        {
            movement->Jump();
        }
        break;

    case InputComponent::Key::OBB_RIGHT:
        movement->MoveRight();
        break;

    default:
        break;
    }
}

void ObbPlayer::OnInputKeyReleased(const InputComponent::Key& key)
{
    Super::OnInputKeyReleased(key);

    Player::GravityPortalState& state = GetGravityPortalState();
    if (state.CurrentSpace != state.LastSpace)
    {
        return;
    }

    MovementComponent* movement = GetMovementComponent();
    if (!movement)
    {
        return;
    }

    switch (key)
    {
    case InputComponent::Key::OBB_UP:
        break;
    case InputComponent::Key::OBB_LEFT:
        break;
    case InputComponent::Key::OBB_DOWN:
        break;
    case InputComponent::Key::OBB_RIGHT:
        break;
    default:
        break;
    }
}

void ObbPlayer::OnInputKeyRepeated(const InputComponent::Key& key)
{
    Super::OnInputKeyRepeated(key);

    Player::GravityPortalState& state = GetGravityPortalState();
    if (state.CurrentSpace != state.LastSpace)
    {
        return;
    }

    MovementComponent* movement = GetMovementComponent();
    if (!movement)
    {
        return;
    }

    switch (key)
    {
    case InputComponent::Key::OBB_UP:
        if (movement->CanJump())
        {
            movement->Jump();
        }
        break;

    case InputComponent::Key::OBB_LEFT:
        movement->MoveLeft();
        break;

    case InputComponent::Key::OBB_DOWN:
        if (movement->CanJump())
        {
            movement->Jump();
        }
        break;

    case InputComponent::Key::OBB_RIGHT:
        movement->MoveRight();
        break;

    default:
        break;
    }
}

void ObbPlayer::Init()
{
    Super::Init();

    // Set the player position at the game start
    TransformComponent* transform = GetComponent<TransformComponent>();
    if (transform)
    {
        transform->SetPosition(vmml::vec2f(200.0f, 100.0f));
    }

    // Reconstruct the player's collision as each player's height is different
    CollisionComponent* collision = GetComponent<CollisionComponent>();
    if (collision)
    {
        collision->SetCenter(vmml::vec2f(1.37f, 1.37f));
        collision->Reconstruct();
    }
}

void ObbPlayer::Update(float delta)
{
    Super::Update(delta);

    InputComponent* input = GetInputComponent();
    if (!input)
    {
        return;
    }

    /* If there is no input try to stop physics based movement (due to
    * unwanted forces from box2d) */
    if (!input->IsKeyDown(InputComponent::Key::OBB_LEFT)
        && !input->IsKeyDown(InputComponent::Key::OBB_RIGHT))
    {
        MovementComponent* movement = GetMovementComponent();
        if (!movement)
        {
            return;
        }

        if (movement->IsMoving())
        {
            movement->Stop();
        }
    }
}