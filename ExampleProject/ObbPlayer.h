/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * The Obb player class
 */

#pragma once

#include <memory>
#include "Player.h"

class ObbPlayer : public Player
{
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl;

public:
	explicit ObbPlayer(Game* game);
	virtual ~ObbPlayer();

protected:
	void OnInputKeyPressed(const InputComponent::Key& key) override;
	void OnInputKeyReleased(const InputComponent::Key& key) override;
	void OnInputKeyRepeated(const InputComponent::Key& key) override;

public:
	void Init() override;
	void Update(float delta) override;
};
