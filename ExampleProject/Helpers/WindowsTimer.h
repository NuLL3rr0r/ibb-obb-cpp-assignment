#pragma once

#include <windows.h>

class WindowsTimer
{
public:
	WindowsTimer();

	virtual ~WindowsTimer();

	void Start();
	void Stop();

	float GetTimeInSeconds();

private:
	LARGE_INTEGER	m_ticksPerSecond;
	LARGE_INTEGER	m_currentTicks;
	LARGE_INTEGER	m_frameDelay;
};
