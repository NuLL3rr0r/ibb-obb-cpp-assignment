/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Platform-specific hacks and macors
 */

#pragma once

#if defined(_WIN32) || defined(_WIN64)
	#define	FORCEINLINE		__forceinline			/* Force code to be inline */
	#define	FORCENOINLINE	__declspec(noinline)	/* Force code to NOT be inline */
#else	/* Other platforms than Windows */
	#if defined(DEBUG_BUILD)
		#define	FORCEINLINE	inline	/* Don't force code to be inline, or you'll run into -Wignored-attributes */
	#else
		#define	FORCEINLINE	inline __attribute__ ((always_inline))	/* Force code to be inline */
	#endif /* defined(DEBUG_BUILD) */
	#define	FORCENOINLINE	__attribute__((noinline))	/* Force code to NOT be inline */
#endif /* defined(_WIN32) || defined(_WIN64) */