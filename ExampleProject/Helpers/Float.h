/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * Floating point related utility macros
 */

#pragma once

#include <cmath>
#include "Helpers/Platform.h"

#define SMALL_NUMBER		(1.e-8f)
#define KINDA_SMALL_NUMBER	(1.e-4f)

class Float
{
public:
	static FORCEINLINE bool IsNearlyEqual(float a, float b, float errorTolerance = SMALL_NUMBER)
	{
		return std::abs(a - b) <= errorTolerance;
	}

	static FORCEINLINE bool IsNearlyZero(float value, float errorTolerance = SMALL_NUMBER)
	{
		return std::abs(value) <= errorTolerance;
	}
};
