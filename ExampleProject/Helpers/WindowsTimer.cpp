
#include "WindowsTimer.h"

WindowsTimer::WindowsTimer()
{
	QueryPerformanceCounter(&m_frameDelay);
	QueryPerformanceCounter(&m_currentTicks);
	QueryPerformanceFrequency(&m_ticksPerSecond);
}

WindowsTimer::~WindowsTimer()
{
}

void WindowsTimer::Start()
{
	QueryPerformanceCounter(&m_currentTicks);
}

void WindowsTimer::Stop()
{
	QueryPerformanceCounter(&m_frameDelay);
}

float WindowsTimer::GetTimeInSeconds()
{
	return (float)(m_frameDelay.QuadPart - m_currentTicks.QuadPart) / ((float)m_ticksPerSecond.QuadPart);
}