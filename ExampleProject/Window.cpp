#include "Window.h"

#include <stdio.h>

Window::Window(int width, int height):
	sdlWindow(NULL),
	width(width),
	height(height)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		sdlWindow = SDL_CreateWindow("My Awesome Gem", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);

		if (sdlWindow == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		}
	}
}

Window::~Window()
{
}

SDL_Window* Window::GetWindow() const
{
	return sdlWindow;
}

vmml::Vector2i Window::GetSize() const
{
	return Vector2i(width, height);
}
