#pragma once

#include <SDL.h>
#include "vmmlib.h"

class Window
{
public:
	Window(int width, int height);
	virtual ~Window();

	SDL_Window* GetWindow() const;

	Vector2i GetSize() const;

private:
	SDL_Window* sdlWindow;

	int width;
	int height;
};