/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * A collision listner which is being used in conjunction with the physics manager
 */

#include "ContactListener.h"
#include "Helpers/loguru.h"
#include "PhysicsManager.h"
#include "IbbPlayer.h"
#include "ObbPlayer.h"

struct ContactListener::Impl
{
	PhysicsManager* PhysicsManager;
}; 

ContactListener::ContactListener(PhysicsManager* physicsManager) :
	b2ContactListener(),
	pimpl(std::make_unique<ContactListener::Impl>())
{
	pimpl->PhysicsManager = physicsManager;
}

ContactListener::~ContactListener()
{

}

void ContactListener::BeginContact(b2Contact* contact)
{
	LOG_F(INFO, "BeginContact");

	if (!contact)
	{
		return;
	}

	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();

	if (!fixtureA || !fixtureB)
	{
		return;
	}

	b2Body* bodyA = fixtureA->GetBody();
	b2Body* bodyB = fixtureB->GetBody();

	if (!bodyA || !bodyB)
	{
		return;
	}

	IbbPlayer* ibbPlayer = static_cast<IbbPlayer*>(bodyA->GetUserData());
	if (ibbPlayer && fixtureB->IsSensor())
	{
		LOG_F(INFO, "IbbPlayer BeginContact");

		pimpl->PhysicsManager->OnPlayerEntersGravityPortal(ibbPlayer);
		return;
	}

	ibbPlayer = static_cast<IbbPlayer*>(bodyB->GetUserData());
	if (ibbPlayer && fixtureA->IsSensor())
	{
		LOG_F(INFO, "IbbPlayer BeginContact");

		pimpl->PhysicsManager->OnPlayerEntersGravityPortal(ibbPlayer);
		return;
	}

	ObbPlayer* obbPlayer = static_cast<ObbPlayer*>(bodyA->GetUserData());
	if (obbPlayer && fixtureB->IsSensor())
	{
		LOG_F(INFO, "ObbPlayer BeginContact");

		pimpl->PhysicsManager->OnPlayerEntersGravityPortal(obbPlayer);
		return;
	}

	obbPlayer = static_cast<ObbPlayer*>(bodyB->GetUserData());
	if (obbPlayer && fixtureA->IsSensor())
	{
		LOG_F(INFO, "ObbPlayer BeginContact");

		pimpl->PhysicsManager->OnPlayerEntersGravityPortal(obbPlayer);
		return;
	}
}

void ContactListener::EndContact(b2Contact* contact)
{
	if (!contact)
	{
		return;
	}

	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();

	if (!fixtureA || !fixtureB)
	{
		return;
	}

	b2Body* bodyA = fixtureA->GetBody();
	b2Body* bodyB = fixtureB->GetBody();

	if (!bodyA || !bodyB)
	{
		return;
	}

	IbbPlayer* ibbPlayer = static_cast<IbbPlayer*>(bodyA->GetUserData());
	if (ibbPlayer && fixtureB->IsSensor())
	{
		LOG_F(INFO, "IbbPlayer EndContact");

		pimpl->PhysicsManager->OnPlayerLeavesGravityPortal(ibbPlayer);
		return;
	}

	ibbPlayer = static_cast<IbbPlayer*>(bodyB->GetUserData());
	if (ibbPlayer && fixtureA->IsSensor())
	{
		LOG_F(INFO, "IbbPlayer EndContact");

		pimpl->PhysicsManager->OnPlayerLeavesGravityPortal(ibbPlayer);
		return;
	}

	ObbPlayer* obbPlayer = static_cast<ObbPlayer*>(bodyA->GetUserData());
	if (obbPlayer && fixtureB->IsSensor())
	{
		LOG_F(INFO, "ObbPlayer EndContact");

		pimpl->PhysicsManager->OnPlayerLeavesGravityPortal(obbPlayer);
		return;
	}

	obbPlayer = static_cast<ObbPlayer*>(bodyB->GetUserData());
	if (obbPlayer && fixtureA->IsSensor())
	{
		LOG_F(INFO, "ObbPlayer EndContact");

		pimpl->PhysicsManager->OnPlayerLeavesGravityPortal(obbPlayer);
		return;
	}
}