#include "Input.h"

bool Input::PollEvent()
{
	if (clearSinglePressData)
	{
		keyboard.Clear();
		clearSinglePressData = false;
	}

	if (SDL_PollEvent(&sdlEvent) > 0)
	{
		keyboard.Update();

		return true;
	}
	else
	{
		clearSinglePressData = true;
		return false;
	}
}
