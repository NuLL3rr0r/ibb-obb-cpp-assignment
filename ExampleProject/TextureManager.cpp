#include "TextureManager.h"
#include "Texture.h"
#include "Helpers\Integer.h"


TextureManager::TextureManager(Renderer& renderer):
	renderer(renderer)
{
	//Reserve(100);
}

TextureManager::~TextureManager()
{
	Clear();
}

//Add & Destroy Textures
Texture* TextureManager::AddTexture(const std::string& filepath)
{

	Texture* texture = new Texture(renderer, filepath);
	textures.push_back(texture);
	return texture;
}

void TextureManager::RemoveTexture(Texture* texture)
{
	for (uint i = 0; i < textures.size(); i++)
	{
		if (textures[i] == texture)
		{
			textures.erase(textures.begin() + i);
			break;
		}
	}
}

Texture* TextureManager::GetTexture(unsigned int key)
{
	if (key > (textures.size() - 1) || textures.empty())
	{
		printf("You can't get the texture because it doesn't exist! You are looking for a texture in unallocated memory! Key: %i \n", key);
		return NULL;
	}

	Texture* texture = textures[key];

	return texture;
}

void TextureManager::Clear()
{
	for (unsigned int i = 0; i < textures.size(); i++)
	{
		if (textures[i] != NULL)
		{
			std::cout << "Destroyed texture: " << i << std::endl;
			textures[i]->Destroy();
			delete textures[i];
			textures[i] = NULL;
		}
	}

	textures.clear();
}