/**
 * @file
 * @author  Mamadou Babaei <info@babaei.net>
 * @version 0.1.0
 *
 * @section DESCRIPTION
 *
 * A generic physics manager which takes care of all physics stuff in the game
 */

#include "PhysicsManager.h"
#include <memory>
#include <cassert>
#include <cmath>
#include "box2d/box2d.h"
#include "Components/CollisionComponent.h"
#include "Components/SpriteComponent.h"
#include "Components/TransformComponent.h"
#include "ContactListener.h"
#include "ECS/Entity.h"
#include "Floor.h"
#include "Game.h"
#include "Helpers/Float.h"
#include "Player.h"
#include "Renderer.h"
#include "Texture.h"

constexpr float SCALE = 30.0f;

struct PhysicsManager::Impl
{
    b2Vec2 Gravity;
    b2World World;

	Game* Game;

	std::unique_ptr<ContactListener> ContactListener;

	Impl():
		Gravity(0.0f, 9.8f),
		World(Gravity)
	{

	}

	/// <summary>
	/// Return whether the current player is in lower or upper part of the screen
	/// </summary>
	/// <param name="player">The player to check their position</param>
	/// <returns>The screen space</returns>
	Player::GravityPortalState::Space GetCurrentSpace(Player* player)
	{
		assert(player);
		assert(Game);

		const TransformComponent* transform = player->GetComponent<TransformComponent>();
		assert(transform);
		const float position = transform->GetPosition().y;

		const Floor& floor = Game->GetFloor();
		const float lowerY = floor.GetLowerSpaceYCoordinate();
		const float upperY = floor.GetUpperSpaceYCoordinate();

		if (position <= upperY)
		{
			return Player::GravityPortalState::Space::Upper;
		}

		if (position >= lowerY)
		{
			return Player::GravityPortalState::Space::Lower;
		}

		// In order to satisfy the compiler
		return Player::GravityPortalState::Space::Upper;
	}

	/// <summary>
	/// Check the current player against the gravity portal and reverse the
	/// gravity if it's required to.
	/// </summary>
	/// <param name="player">The player to check against the portal</param>
	void GravityPortalCheck(Player* player)
	{
		Player::GravityPortalState& state = player->GetGravityPortalState();
		if (state.CurrentSpace == state.LastSpace)
		{
			return;
		}

		if (!player || !Game)
		{
			return;
		}

		const SpriteComponent* sprite = player->GetComponent<SpriteComponent>();
		if (!sprite)
		{
			return;
		}

		Texture* texture = sprite->GetTexture();
		if (!texture)
		{
			return;
		}

		TransformComponent* transform = player->GetComponent<TransformComponent>();
		if (!transform)
		{
			return;
		}

		const vmml::vec2f position = transform->GetPosition();

		const Floor& floor = Game->GetFloor();
		const float maxYTraverseDistance = 100.0f;
		const float spriteHeight = static_cast<float>(texture->GetSize().y);

		if (state.CurrentSpace == Player::GravityPortalState::Space::Upper)
		{
			const float upperY = floor.GetUpperSpaceYCoordinate();
			if (std::abs(position.y - (upperY - spriteHeight)) > maxYTraverseDistance)
			{
				ReverseGravity(player);
				state.LastSpace = state.CurrentSpace;
			}
		}
		else
		{
			const float lowerY = floor.GetLowerSpaceYCoordinate();
			if (std::abs(position.y - lowerY) > maxYTraverseDistance)
			{
				ReverseGravity(player);
				state.LastSpace = state.CurrentSpace;
			}
		}
	}

	/// <summary>
	/// Calculate the landing location of the player after exiting the portal
	/// and apply the necessary force to land the player at the best spot based
	/// on the entering direction
	/// </summary>
	/// <param name="player">The player to check against the portal</param>
	void GravityPortalLandingCheck(Player* player)
	{
		Player::GravityPortalState& state = player->GetGravityPortalState();
		if (state.CurrentSpace != state.LastSpace)
		{
			return;
		}

		if (!player || !Game)
		{
			return;
		}

		CollisionComponent* collision = player->GetComponent<CollisionComponent>();
		if (!collision)
		{
			return;
		}

		TransformComponent* transform = player->GetComponent<TransformComponent>();
		if (!transform)
		{
			return;
		}

		const vmml::vec2f position = transform->GetPosition();

		if (state.CurrentSpace == state.LastSpace
			&& state.LandingSpot != vmml::vec2f::ZERO)
		{
			if (state.EnteringXVelocity > 0.0f)
			{
				if (position.x >= state.LandingSpot.x)
				{
					vmml::vec2f velocity(collision->GetLinearVelocity());
					velocity.x = 0;
					collision->SetLinearVelocity(velocity);
					state.LandingSpot = vmml::vec2f::ZERO;
				}
			}
			else if (state.EnteringXVelocity < 0.0f)
			{
				if (position.x <= state.LandingSpot.x)
				{
					vmml::vec2f velocity(collision->GetLinearVelocity());
					velocity.x = 0;
					collision->SetLinearVelocity(velocity);
					state.LandingSpot = vmml::vec2f::ZERO;
				}
			}
		}
	}

	/// <summary>
	/// Reverse the gravity for the specified player
	/// </summary>
	/// <param name="player">The player to reverse the gravity for</param>
	void ReverseGravity(Player* player)
	{
		if (!player || !Game)
		{
			return;
		}

		CollisionComponent* collision = player->GetComponent<CollisionComponent>();
		if (!collision)
		{
			return;
		}

		b2Body* body = collision->GetBody(player);
		if (!body)
		{
			return;
		}

		TransformComponent* transform = player->GetComponent<TransformComponent>();
		if (!transform)
		{
			return;
		}

		const float gravityScale = body->GetGravityScale();

		body->SetGravityScale(gravityScale * -1.0f);
		body->SetAngularVelocity(0.0f);
		body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));

		const Floor& floor = Game->GetFloor();

		vmml::vec2f playerLocation = transform->GetPosition();

		Player::GravityPortalState& state = player->GetGravityPortalState();
		state.LandingSpot = vmml::vec2f::ZERO;

		if (std::abs(state.EnteringXVelocity) < 0.1f)
		{

		}
		else if (state.EnteringXVelocity > 0.0f)
		{
			if (state.CurrentSpace == Player::GravityPortalState::Space::Upper)
			{
				vmml::vec2f targetLocation(floor.GetGPUpperRightCoordinates() + vmml::vec2f(150.0f, 0.0f));
				vmml::vec2f targetDirection((targetLocation - playerLocation).getNormalized());
				vmml::vec2f impulse(targetDirection * 100.0f);
				state.LandingSpot = targetLocation;
				collision->ApplyLinearImpulse(impulse);
			}
			else
			{
				vmml::vec2f targetLocation(floor.GetGPLowerRightCoordinates() + vmml::vec2f(150.0f, 0.0f));
				vmml::vec2f targetDirection((targetLocation - playerLocation).getNormalized());
				vmml::vec2f impulse(targetDirection * 100.0f);
				state.LandingSpot = targetLocation;
				collision->ApplyLinearImpulse(impulse);
			}
		}
		else if (state.EnteringXVelocity < 0.0f)
		{
			if (state.CurrentSpace == Player::GravityPortalState::Space::Upper)
			{
				vmml::vec2f targetLocation(floor.GetGPUpperLeftCoordinates() + vmml::vec2f(-150.0f, 0.0f));
				vmml::vec2f targetDirection((targetLocation - playerLocation).getNormalized());
				vmml::vec2f impulse(targetDirection * 100.0f);
				state.LandingSpot = targetLocation;
				collision->ApplyLinearImpulse(impulse);
			}
			else
			{
				vmml::vec2f targetLocation(floor.GetGPLowerLeftCoordinates() + vmml::vec2f(-150.0f, 0.0f));
				vmml::vec2f targetDirection((targetLocation - playerLocation).getNormalized());
				vmml::vec2f impulse(targetDirection * 100.0f);
				state.LandingSpot = targetLocation;
				collision->ApplyLinearImpulse(impulse);
			}
		}
	}
};

PhysicsManager::PhysicsManager(Game* game) :
    pimpl(std::make_unique<PhysicsManager::Impl>())
{
	pimpl->Game = game;
}

PhysicsManager::~PhysicsManager() = default;

void PhysicsManager::OnPlayerEntersGravityPortal(Player* player)
{
	if (!player)
	{
		return;
	}

	CollisionComponent* collision = player->GetComponent<CollisionComponent>();
	if (!collision)
	{
		return;
	}

	b2Body* body = collision->GetBody(player);
	if (!body)
	{
		return;
	}

	b2Vec2 velocity = body->GetLinearVelocity();

	Player::GravityPortalState& state = player->GetGravityPortalState();
	state.EnteringXVelocity = velocity.x;
	state.LandingSpot = vmml::vec2f::ZERO;
}

void PhysicsManager::OnPlayerLeavesGravityPortal(Player* player)
{
	if (!player)
	{
		return;
	}

	Player::GravityPortalState& state = player->GetGravityPortalState();
	state.LastSpace = state.CurrentSpace;
	state.CurrentSpace = pimpl->GetCurrentSpace(player);
}

b2World& PhysicsManager::GetWorld() const
{
	return pimpl->World;
}

void PhysicsManager::Init()
{
	pimpl->ContactListener = std::make_unique<ContactListener>(this);
	pimpl->World.SetContactListener(pimpl->ContactListener.get());
}

void PhysicsManager::Update(float delta)
{
	/* Iterate over all collisions in the box2d world and adjust the attached
	* sprites according to the box2d collision's position; moreover, handle
	* gravity portal collisions and transitions or reverse gravity according to
	* the situation */
	for (b2Body* bodyIterator = pimpl->World.GetBodyList();
		bodyIterator != 0;
		bodyIterator = bodyIterator->GetNext())
	{
		if (bodyIterator->GetType() == b2_dynamicBody)
		{
			Entity* attachedObject = static_cast<Entity*>(bodyIterator->GetUserData());
			if (attachedObject)
			{
				TransformComponent* transform = attachedObject->GetComponent<TransformComponent>();
				if (transform)
				{
					vmml::vec2f position(
						SCALE * bodyIterator->GetPosition().x,
						SCALE * bodyIterator->GetPosition().y);
					transform->SetPosition(position);
				}

				Player* player = static_cast<Player*>(attachedObject);
				if (player)
				{
					pimpl->GravityPortalCheck(player);
					pimpl->GravityPortalLandingCheck(player);
				}
			}
		}
	}

	pimpl->World.Step(1 / 60.f, 8, 3);
}

void PhysicsManager::DebugDraw(const Renderer& renderer)
{
	// Iterate over all collisions in the box2d world and draw them
	for (b2Body* bodyIterator = pimpl->World.GetBodyList();
		bodyIterator != 0;
		bodyIterator = bodyIterator->GetNext())
	{
		b2Vec2 position(bodyIterator->GetPosition());
		for (b2Fixture* fixture = bodyIterator->GetFixtureList();
			fixture != 0; fixture = fixture->GetNext())
		{
			b2Shape* shape = fixture->GetShape();
			if (!shape)
			{
				return;
			}

			b2Shape::Type type = shape->GetType();

			if (type == b2Shape::Type::e_polygon)
			{
				b2PolygonShape* polygon = static_cast<b2PolygonShape*>(shape);
				if (!polygon)
				{
					return;
				}

				b2Vec2 position(bodyIterator->GetPosition());

				for (const b2Vec2& size : polygon->m_vertices)
				{
					SDL_Rect rect;
					rect.x = static_cast<int>(position.x * SCALE);
					rect.y = static_cast<int>(position.y * SCALE);
					rect.w = static_cast<int>(size.x * SCALE);
					rect.h = static_cast<int>(size.y * SCALE);

					SDL_SetRenderDrawColor(renderer.GetRenderer(), 255, 255, 255, 255);
					SDL_RenderDrawRect(renderer.GetRenderer(), &rect);
				}
			}
		}
	}
}

void PhysicsManager::CreateGround(const vmml::vec2f& size, const vmml::vec2f& position,
	bool bIsGravityPortal)
{
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(
		(position.x + (size.x / 2.0f)) / SCALE,
		(position.y + (size.y / 2.0f)) / SCALE);
	bodyDef.type = b2_staticBody;
	b2Body* body = pimpl->World.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox((size.x / 2.0f) / SCALE, (size.y / 2.0f) / SCALE);
	b2FixtureDef fixtureDef;
	fixtureDef.density = 0.0f;
	fixtureDef.isSensor = bIsGravityPortal;
	fixtureDef.shape = &shape;
	body->CreateFixture(&fixtureDef);
}

void PhysicsManager::CreateBox(
	void* attachedObject,
	const vmml::vec2f& size, const vmml::vec2f& position,
	const vmml::vec2f& center, float density, float friction)
{
	b2BodyDef bodyDef;
	bodyDef.userData = attachedObject;
	bodyDef.position = b2Vec2(position.x / SCALE, position.y / SCALE);
	bodyDef.type = b2_dynamicBody;
	b2Body* body = pimpl->World.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox((size.x / 2.0f) / SCALE, (size.y / 2.0f) / SCALE,
		b2Vec2(center.x, center.y), 0.0f);
	b2FixtureDef fixtureDef;
	fixtureDef.density = density;
	fixtureDef.friction = friction;
	fixtureDef.shape = &shape;
	body->CreateFixture(&fixtureDef);
}