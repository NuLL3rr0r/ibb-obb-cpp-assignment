The purpose is to create a basic ibb & obb gameplay.

For info about the gameplay watch this video: https://www.youtube.com/watch?v=2iif636QAcc


Basic Assignment:
The game should have 2 characters
Characters can jump
Characters don't have collision with eachother
Characters can't fall through the floor
Gravity below the floor is reversed

Extra:
The two characters should have different heights
Characters have collision with each other
Characters can stand on top of each other while jumping


We will judge the code based on the following criteria:
+ Simplicity. 
+ Clarity.
+ Object-Oriented design
+ Are there any unnecessary abstractions?
+ Are the algorithms used as simple as possible?
+ Is it easy to follow the flow of the code?
+ Is it easy to understand what the code does?
+ Do the comments really add value?


Feel free to send any questions to maurice@codeglue.com

To add new images (only PNGs are supported), just add them to the content folder.